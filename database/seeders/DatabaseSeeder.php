<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PositionsTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(UserPositionTableSeeder::class);
        $this->call(UserDepartmentTableSeeder::class);
    }
}
