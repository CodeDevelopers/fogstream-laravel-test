<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Position;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::truncate();
        $data = array();
    	$data['name'] = 'admin';
    	$data['description'] = 'Admin - has all the rights';
        Position::create($data);
        $data = array();
    	$data['name'] = 'manager';
    	$data['description'] = 'Manager - can change data, add data, but cannot delete records';
        Position::create($data);
        $data = array();
    	$data['name'] = 'user';
    	$data['description'] = 'User - can only view data';
        Position::create($data);
    }
}
