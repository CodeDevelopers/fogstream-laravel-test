<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::truncate();
        $data = array();
    	$data['departmant_name'] = 'Department 1';
    	$data['departmant_desctiption'] = 'Department 1 Description';
        Department::create($data);
        $data = array();
    	$data['departmant_name'] = 'Department 2';
    	$data['departmant_desctiption'] = 'Department 2 Description';
        Department::create($data);
        $data = array();
    	$data['departmant_name'] = 'Department 3';
    	$data['departmant_desctiption'] = 'Department 3 Description';
        Department::create($data);
    }
}
