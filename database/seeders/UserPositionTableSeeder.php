<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserPosition;

class UserPositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserPosition::truncate();
        $data = array();
    	$data['user_id'] = 1;
    	$data['position_id'] = 1;
        UserPosition::create($data);
        $data['user_id'] = 2;
    	$data['position_id'] = 2;
        UserPosition::create($data);
        $data['user_id'] = 3;
    	$data['position_id'] = 3;
        UserPosition::create($data);
    }
}
