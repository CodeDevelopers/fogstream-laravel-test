<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::truncate();
        $user = array();
    	$user['name'] = 'Niklesh Raut';
    	$user['email'] = 'niklesh@gmail.com';
    	$user['email_verified_at'] = date('Y-m-d');
    	$user['password'] = Hash::make("12345678");
        User::create($user);
        $user = array();
    	$user['name'] = 'Rishi Raut';
    	$user['email'] = 'rishi@gmail.com';
    	$user['email_verified_at'] = date('Y-m-d');
    	$user['password'] = Hash::make("12345678");
        User::create($user);
        $user = array();
    	$user['name'] = 'Rishabh Raut';
    	$user['email'] = 'rishabh@gmail.com';
    	$user['email_verified_at'] = date('Y-m-d');
    	$user['password'] = Hash::make("12345678");
        User::create($user);
    }
}
