<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserDepartment;

class UserDepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserDepartment::truncate();
        $data = array();
    	$data['user_id'] = 1;
    	$data['department_id'] = 1;
        UserDepartment::create($data);
        $data = array();
    	$data['user_id'] = 1;
    	$data['department_id'] = 2;
        UserDepartment::create($data);
        $data = array();
    	$data['user_id'] = 1;
    	$data['department_id'] = 3;
        UserDepartment::create($data);
        $data['user_id'] = 2;
    	$data['department_id'] = 2;
        UserDepartment::create($data);
        $data['user_id'] = 2;
    	$data['department_id'] = 3;
        UserDepartment::create($data);
        $data['user_id'] = 3;
    	$data['department_id'] = 3;
        UserDepartment::create($data);
    }
}
