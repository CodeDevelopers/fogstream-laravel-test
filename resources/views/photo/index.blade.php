@extends('layouts.app')

@section('content')

<div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-body">
                <!-- Zero configuration table -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Upload Images</h4>
                                    <div class="heading-elements">
                                        <a href="{{ route('photo.create') }}" class="btn btn-outline-primary btn-min-width btn-lg"><i class="feather icon-plus" ></i>  Upload Photo</a>
                                    </div>
                                </div>
                                @if ($message = Session::get('success'))
							        <div class="alert alert-success">
							            <p>{{ $message }}</p>
							        </div>
							    @endif
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-striped table-bordered zero-configuration">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th>Image</th>
                                                    <th>Upload Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	@foreach ($photos as $photo)
                                                <tr>
                                                	<td>{{ ++$i }}</td>
										            <td><img src="/images/{{ $photo->photo_name }}" style="width:50px; height:50px;"> </td>
										            <td>{{ $photo->created_at }}</td>
										            <td>
										                <form action="{{ route('photo.destroy',$photo->id) }}" method="POST">
										                  @if( in_array($userRole, ['admin','manager','user']) )
										                    <a class="btn btn-info" href="{{ route('photo.show',$photo->id) }}">Show</a>
										                  @endif
                                                          @if( in_array($userRole, ['admin','manager']) )
										                    <a class="btn btn-primary" href="{{ route('photo.edit',$photo->id) }}">Edit</a>
										                  @endif
                                                          @if( in_array($userRole, ['admin']) )
										                    @csrf
										                    @method('DELETE')
										      
										                    <button type="submit" class="btn btn-danger">Delete</button>
                                                          @endif
										                </form>
										            </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->
                
                

            </div>
        </div>
    </div>
@endsection
