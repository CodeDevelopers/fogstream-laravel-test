<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Photo;
use App\Models\User;
use Auth;
class PhotoController extends Controller
{
    public function getUserRole(){
        $user = User::find(Auth::id());
        return $user->user_position->position->name ?? '';
    } 
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::latest()->paginate(5);
        if( !in_array($this->getUserRole(), ['admin','manager','user']) ){
            return view('404');
        }
        return view('photo.index',compact('photos'))
            ->with('i', (request()->input('page', 1) - 1) * 5)->with('userRole',$this->getUserRole());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if( !in_array($this->getUserRole(), ['admin','manager']) ){
            return view('404');
        }
        return view('photo.create');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !in_array($this->getUserRole(), ['admin','manager']) ){
            return view('404');
        }
    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
        $data['photo_name'] = $imageName;
  
        /* Store $imageName name in DATABASE from HERE */
    	Photo::create($data);
    	return redirect()->route('photo.index')
                        ->with('success','Photo uploaded successfully.');
        
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        if( !in_array($this->getUserRole(), ['admin','manager','user']) ){
            return view('404');
        }
        return view('photo.show',compact('photo'));
    } 
     
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        if( !in_array($this->getUserRole(), ['admin','manager']) ){
            return view('404');
        }
        return view('photo.edit',compact('photo'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
    	if( !in_array($this->getUserRole(), ['admin','manager']) ){
            return view('404');
        }
    	$request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        $imageName = time().'.'.$request->image->extension();  
     
        $request->image->move(public_path('images'), $imageName);
        $data['photo_name'] = $imageName;
        $photo->update($data);
    
        return redirect()->route('photo.index')
                        ->with('success','Photo updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        if( !in_array($this->getUserRole(), ['admin']) ){
            return view('404');
        }
        $photo->delete();
    
        return redirect()->route('photo.index')
                        ->with('success','Photo deleted successfully');
    }
}
